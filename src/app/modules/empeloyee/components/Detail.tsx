/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/alt-text */
import React, { FC, useEffect, useRef, useState } from 'react'
import { StepperComponent } from '../../../../_metronic/assets/ts/components'
import { toAbsoluteUrl } from '../../../../_metronic/helpers/AssetHelpers'
import { useNavigate } from 'react-router-dom'
import clsx from 'clsx'

type Props = {
  disabled: boolean,
}

const Detail: React.FC<Props> = ({ disabled }) => {

  const navigate = useNavigate()
  const stepperRef = useRef<HTMLDivElement | null>(null)
  const stepper = useRef<StepperComponent | null>(null)
  const [tab, setTab] = useState('Account Detail')



  const loadStepper = () => {
    stepper.current = StepperComponent.createInsance(stepperRef.current as HTMLDivElement)
  }

  useEffect(() => {
    if (!stepperRef.current) {
      return
    }

    loadStepper()
  }, [stepperRef])

  return (
    <>
      <div
        ref={stepperRef}
        className='stepper stepper-pills stepper-column d-flex flex-column flex-xl-row flex-row-fluid'
        id='kt_create_account_stepper'
      >
        <div className='d-flex justify-content-center flex-column align-items-center bg-white rounded justify-content-xl-start flex-row-auto w-100 w-xl-300px w-xxl-400px me-9'>
          <div className='px-6 px-lg-10 px-xxl-15 py-20 d-flex flex-column'>
            <img className='rounded mb-5' src={toAbsoluteUrl('/media/icon/def-user.svg')} />
            <h1 className='mt-5 text-center'>Jamal Solehun</h1>
            <h2 className='mt-5 text-center text-muted'>Marketing</h2>

            <h2 className='mt-5 text-center'>Email</h2>
            <text className='text-center mb-5'>Open@gmail.com</text>
            <h2 className='mt-5 text-center'>Phone</h2>
            <text className='text-center mb-5'>+6281234567890	</text>
          </div>
        </div>

        <div className='d-flex p-5 bg-white rounded' style={{ width: '100%' }}>
          <div className='row mt-5'>
            <div className='card'>
              <div className='card-header'>
                <div className='row'>
                  <div className='col-md-12'>
                    <ul
                      className='nav nav-stretch nav-line-tabs fw-bold border-transparent flex-nowrap'
                      role='tablist'
                    >
                      <li className='nav-item'>
                        <a
                          className={clsx(`nav-link cursor-pointer`, {
                            active: tab === 'Account Detail',
                          })}
                          onClick={() => setTab('Account Detail')}
                          role='tab'
                        >
                          Account Detail
                        </a>
                      </li>

                      <li className='nav-item'>
                        <a
                          className={clsx(`nav-link cursor-pointer`, {
                            active: tab === 'Role Setting',
                          })}
                          onClick={() => setTab('Role Setting')}
                          role='tab'
                        >
                          Role Setting
                        </a>
                      </li>
                      <li className='nav-item'>
                        <a
                          className={clsx(`nav-link cursor-pointer`, {
                            active: tab === 'Password Setting',
                          })}
                          onClick={() => setTab('Password Setting')}
                          role='tab'
                        >
                          Password Setting
                        </a>
                      </li>
                    </ul>
                    <form className='form'>
                      <div className='card-body'>
                        <div className='tab-content pt-3'>
                          <div className={clsx('tab-pane', { active: tab === 'Account Detail' })}>
                            <div className='row mb-12'>
                              <div className='col-lg-12'>
                                <div className='row mb-5'>
                                  <div className='col-lg-6 fv-row'>
                                    <label className='col-lg-6 col-form-label required fw-bold fs-6'>Nama Depan</label>
                                    <input
                                      disabled={disabled}
                                      type='text'
                                      className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                                    />
                                  </div>
                                  <div className='col-lg-6 fv-row'>
                                    <label className='col-form-label required fw-bold fs-6'>Nama Belakang</label>
                                    <input
                                      disabled={disabled}
                                      type='text'
                                      className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                                    />
                                  </div>
                                </div>
                                <div className='row mb-5'>
                                  <div className='col-lg-6 fv-row'>
                                    <label className='col-lg-6 col-form-label required fw-bold fs-6'>Alamat Lengkap</label>
                                    <textarea
                                      disabled={disabled}
                                      className='form-control form-control-lg form-control-solid mb-3 mb-lg-0' rows={5}
                                    />
                                  </div>
                                  <div className='col-lg-6'>
                                    <div className='row mb-5'>
                                      <div className='col-lg-12 fv-row'>
                                        <label className='col-lg-12 col-form-label required fw-bold fs-6'>Email</label>
                                        <input
                                          disabled={disabled}
                                          type='email'
                                          className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                                        />
                                      </div>
                                      <div className='col-lg-12S fv-row'>
                                        <label className='col-form-label required fw-bold fs-6'>Nomor Telepon</label>
                                        <input
                                          disabled={disabled}
                                          type='tel'
                                          className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className={clsx('tab-pane', { active: tab === 'Role Setting' })}>
                            <div className='row mb-12'>
                              <div className='col-lg-12'>
                                <div className='row mb-5'>
                                  <div className='col-lg-12'>
                                    <div className='row mb-5'>
                                      <div className='col-lg-12 fv-row'>
                                        <label className='col-form-label required fw-bold fs-6'>Position</label>
                                        <div className='col-lg-12 fv-row'>
                                          <select
                                            disabled={disabled}
                                            className='form-select form-select-solid form-select-lg fw-bold'>
                                            <option value=''>Postion</option>
                                          </select>
                                        </div>
                                      </div>
                                      <div className='col-lg-12 fv-row'>
                                        <label className='col-form-label fw-bold fs-6'></label>
                                        <input
                                          style={{
                                            visibility: 'hidden'
                                          }}
                                          disabled={disabled}
                                          type='password'
                                          className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                                        />
                                      </div>
                                      <div className='col-lg-12 fv-row'>
                                        <label className='col-form-label fw-bold fs-6'></label>
                                        <input
                                          style={{
                                            visibility: 'hidden'
                                          }}
                                          disabled={disabled}
                                          type='password'
                                          className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className={clsx('tab-pane', { active: tab === 'Password Setting' })}>
                            <div className='row mb-12'>
                              <div className='col-lg-12'>
                                <div className='row mb-5'>
                                  <div className='col-lg-12'>
                                    <div className='row mb-5'>
                                      <div className='col-lg-12 fv-row'>
                                        <label className='col-form-label required fw-bold fs-6'>New Password</label>
                                        <input
                                          type='password'
                                          disabled={disabled}
                                          className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                                        />
                                      </div>
                                      <div className='col-lg-12 fv-row'>
                                        <label className='col-form-label required fw-bold fs-6'>Confirmation Password</label>
                                        <input
                                          type='password'
                                          disabled={disabled}
                                          className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                                        />
                                      </div>
                                      <div className='m-2'></div>
                                      <div className='col-lg-12 fv-row'>
                                        <input
                                          style={{
                                            visibility: 'hidden'
                                          }}
                                          disabled={disabled}
                                          type='password'
                                          className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                                        />
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                    {/* begin::Footer */}
                    <div className='card-footer py-6'>
                      <div className='row'>
                        <div className="mt-10 d-flex justify-content-between">
                          <button className='btn btn-primary align-items-end' style={{

                          }}>
                            <span className='indicator-label'>Update</span>
                          </button>
                          <button className='btn btn-transparent align-items-end' style={{
                            backgroundColor: 'rgba(208, 156, 10, 0.30)',
                          }}>
                            <text className='text-muted'>Delete Empeloyee</text>
                          </button>
                        </div>
                      </div>
                    </div>
                    {/* end::Footer */}
                  </div>
                </div>
              </div>
            </div >
          </div>
        </div>
      </div>
    </>
  )
}

export { Detail }
