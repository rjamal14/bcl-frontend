/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react'
import { KTSVG } from '../../../../_metronic/helpers'
import { toAbsoluteUrl } from '../../../../_metronic/helpers'
import { Link } from 'react-router-dom'

type Props = {
  className: string
}

const Lists: React.FC<Props> = ({ className }) => {
  return (
    <div className={`card ${className}`}>
      <div className='card-body py-3'>
        <div className='table-responsive'>
          <table className='table table-row-bordered table-row-gray-100 align-middle gs-0 gy-3'>
            <thead>
              <tr className='fw-bolder text-muted'>
                <th className='min-w-50px'>Photo</th>
                <th className='min-w-140px'>ID</th>
                <th className='min-w-120px'>Name</th>
                <th className='min-w-120px'>Position</th>
                <th className='min-w-120px'>Email</th>
                <th className='min-w-120px'>Phone</th>
                <th className='min-w-120px'>Status</th>
                <th className='min-w-80px'>View</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>
                  <a href='#' className='text-dark fw-bolder text-hover-primary fs-6'>
                    <img className='rounded' src={toAbsoluteUrl('/media/avatars/300-1.jpg')} width="48px" />
                  </a>
                </td>
                <td>#00123</td>
                <td>Jamal Solihun</td>
                <td>Marketing</td>
                <td>open@gmail.com</td>
                <td>+6281234567890</td>
                <td>Aktif</td>
                <td>
                  <Link to='detail' style={{ background: '#D09C0A' }} className='btn btn-icon btn-active-color-primary btn-sm'>
                    <KTSVG path='/media/icons/duotune/eyes/eyes.svg' className='svg-icon-3' />
                  </Link>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}

export { Lists }
