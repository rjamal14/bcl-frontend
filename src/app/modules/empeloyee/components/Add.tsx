/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react'
import { useNavigate } from 'react-router-dom'
import { KTSVG } from '../../../../_metronic/helpers/components/KTSVG'

type Props = {
  className: string
}

const Add: React.FC<Props> = ({ className }) => {
  const navigate = useNavigate()

  return (
    <div className={`card ${className}`}>
      {/* begin::Body */}
      <div className='card-body py-5'>
        <div className='card-title m-0'>
          <h3 className='fw-bolder m-0'>Employee Form</h3>
          <p>Lengkapi dan isi data diri sesuai dengan identitas anda</p>
        </div>

        <div className='row mb-6'>

          <div className='col-lg-12'>
            <div className='row mb-5'>
              <div className='col-lg-6 fv-row'>
                <label className='col-lg-6 col-form-label required fw-bold fs-6'>Nama Depan</label>
                <input
                  type='text'
                  className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                />
              </div>
              <div className='col-lg-6 fv-row'>
                <label className='col-form-label required fw-bold fs-6'>Nama Belakang</label>
                <input
                  type='text'
                  className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                />
              </div>
            </div>
            <div className='row mb-5'>
              <div className='col-lg-6 fv-row'>
                <label className='col-lg-6 col-form-label required fw-bold fs-6'>Email</label>
                <input
                  type='email'
                  className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                />
              </div>
              <div className='col-lg-6 fv-row'>
                <label className='col-form-label required fw-bold fs-6'>Nomor Telepon</label>
                <input
                  type='tel'
                  className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                />
              </div>
            </div>
            <div className='row mb-5'>
              <div className='col-lg-6 fv-row'>
                <label className='col-lg-6 col-form-label required fw-bold fs-6'>Alamat Lengkap</label>
                <textarea
                  className='form-control form-control-lg form-control-solid mb-3 mb-lg-0' rows={5}
                />
              </div>
              <div className='col-lg-6 fv-row'>
                <label className='col-form-label required fw-bold fs-6'>Position</label>
                <div className='col-lg-12 fv-row'>
                  <select className='form-select form-select-solid form-select-lg fw-bold'>
                    <option value=''>Postion</option>
                  </select>
                </div>
              </div>
            </div>
            <div className='row'>
              <div className='col-lg-6 fv-row'>
                <label className='col-lg-6 col-form-label required fw-bold fs-6'>Foto Employee</label>
                <input
                  type='file'
                  className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                />
              </div>
            </div>

            <div className="text-end mt-10">
              <button className='btn btn-white btn-active-light-primary me-5' onClick={() => navigate(-1)}>Cancel</button>
              <button className='btn btn-primary'>
                <span className='indicator-label'>Add</span> <KTSVG path='/media/icon/add-filled.svg' className='svg-icon-3' />
              </button>
            </div>
          </div>
        </div>
      </div>
      {/* begin::Body */}
    </div>
  )
}

export { Add }
