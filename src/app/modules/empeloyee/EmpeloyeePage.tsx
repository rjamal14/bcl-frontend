import { Route, Routes, Outlet, useNavigate } from "react-router-dom";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Lists } from "./components/Lists";
import { Add } from "./components/Add";
import { Detail } from "./components/Detail";
import Breadcumbs from "../../../_metronic/layout/components/Breadcumbs";
import { KTSVG } from "../../../_metronic/helpers";

const breadcumbs = [
  {
    title: "Super Admin",
    path: "",
  },
  {
    title: "Employee",
    path: "",
  },
];

const breadcumbsAdd = [
  {
    title: "Super Admin",
    path: "",
  },
  {
    title: "Employee List",
    path: "",
  },
  {
    title: "Employee Add",
    path: "",
  },
];

const breadcumbsDetail = [
  {
    title: "Super Admin",
    path: "",
  },
  {
    title: "Employee List",
    path: "",
  },
  {
    title: "Detail Employee",
    path: "",
  },
];

const EmpeloyeePage = () => {
  const navigate = useNavigate();
  const [disabled, setDisabled] = useState(true);

  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path="/"
          element={
            <>
              <Breadcumbs
                data={breadcumbs}
                components={
                  <Link className="btn btn-primary w-100" to="add">
                    <KTSVG
                      path="/media/icon/Add-User.svg"
                      className="svg-icon-3"
                    />{" "}
                    <span className="indicator-label"> Add Employee</span>
                  </Link>
                }
              />
              <Lists className="mb-5 mb-xl-12" />
            </>
          }
        />
        <Route
          path="/add"
          element={
            <>
              <Breadcumbs
                data={breadcumbsAdd}
                components={
                  <Link
                    to="/superadmin/empeloyee"
                    className="btn btn-primary w-100"
                  >
                  <KTSVG path='/media/icon/arrow-back.svg' className='svg-icon-3' /> <span className="indicator-label">Back</span>
                  </Link>
                }
              />
              <Add className="mb-5 mb-xl-12" />
            </>
          }
        />
        <Route
          path="/detail"
          element={
            <>
              <Breadcumbs
                data={breadcumbsDetail}
                components={
                  <div className="d-flex row-column">
                    {disabled ? (
                      <button
                        onClick={() => {
                          setDisabled(false);
                        }}
                        className="btn btn-primary"
                        style={{ marginRight: 20, width: "200px" }}
                      >
                        {" "}
                        <KTSVG
                          path="/media/icon/Add-User.svg"
                          className="svg-icon-3"
                        />
                        <span className="indicator-label"> Edit Empeloyee</span>
                      </button>
                    ) : (
                      <button
                        className="btn btn-white btn-active-light-primary me-5"
                        onClick={() => {
                          setDisabled(true);
                        }}
                      >
                        Cancel
                      </button>
                    )}
                    <Link
                      to="/superadmin/empeloyee"
                      className="btn btn-primary"
                    >
                      <KTSVG path='/media/icon/arrow-back.svg' className='svg-icon-3' /> <span className="indicator-label">Back</span>
                    </Link>
                  </div>
                }
              />
              <Detail disabled={disabled} />
            </>
          }
        />
        {/* <Route index element={<Navigate to='/crafted/widgets/lists' />} /> */}
      </Route>
    </Routes>
  );
};

export default EmpeloyeePage;
