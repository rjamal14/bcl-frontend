/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { KTSVG } from '../../../../_metronic/helpers/components/KTSVG'
import Breadcumbs from '../../../../_metronic/layout/components/Breadcumbs'
import clsx from 'clsx'
import { Modal } from 'react-bootstrap'
import { toAbsoluteUrl } from '../../../../_metronic/helpers/AssetHelpers'

type Props = {
  className: string
}


const breadcumbs = [
  {
    title: 'Super Admin',
    path: '',
  },
  {
    title: 'List Project',
    path: '',
  },
  {
    title: 'Siteplan',
    path: '',
  },
  {
    title: 'Variant',
    path: '',
  },
]

const DetailSitePlan: React.FC<Props> = ({ className }) => {
  const navigate = useNavigate()
  const [dataID, setDataID] = useState<string[]>([])
  const [siteMaps, setSiteMaps] = useState('')
  const [Blok, setBlok] = useState<string[]>([])
  const [Count, setCount] = useState<number>(0)
  const inputRef = React.useRef<HTMLInputElement>(null)
  const [tab, setTab] = useState('Unit')
  const [showModal, setShowModal] = useState(false)
  const [showModalUnit, setShowModalUnit] = useState(false)


  function someHandler(value: string) {
    var id = value;
    var svgState = document.querySelector("#" + id);
    if (svgState !== null) {
      svgState.classList.add("hover");
    }
  }

  useEffect(() => {
    console.log('Blok', Blok);
  }, [Blok, Count])

  function removeAllHover() {
    var svgStates = document.querySelectorAll("#states > *");
    svgStates.forEach(function (el) {
      el.classList.remove("hover");
    });
  }

  function handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    const data = e.target.files;
    if (data !== null && data.length > 0) {
      const fileReader = new FileReader();
      fileReader.readAsText(data[0], "UTF-8");
      fileReader.onload = (res) => {
        var patern = res !== null ? res.target?.result : "";
        if (patern !== null && patern !== undefined) {
          setSiteMaps(String(patern));
          var temp = String(patern).split("<");
          let SetID: string[] = [];
          temp.map((value, index) => {
            if (temp[index].substring(0, 7) === "polygon") {
              let a = String(value).split("fill")[0];
              SetID.push(String(a).split('"')[1])
            }
          });
          setDataID(SetID)
        }
      }
    }
  }

  function CountPercent(id: string) {
    const total = dataID.length;
    const bagian = Blok.filter((o) => o === id).length;
    return bagian / total * 100 + '%';
  }

  function CountUnit(id: string) {
    return Blok.filter((o) => o === id).length;
  }

  function renderSiteMaps() {
    if (dataID.length > 0) {

      const unique = (value: any, index: any, self: string | any[]) => {
        return self.indexOf(value) === index
      }

      const uniqueBlok = Blok.filter(unique)

      console.log(uniqueBlok)

      return (
        <div className={`card ${className}`}>
          <div className='card-body py-3'>
            <div className='row'>
              <div className='col-md-3'>
                <h4 className='mt-5 mb-5'>Properties Map Location</h4>

                {uniqueBlok.map((val) => {
                  return (
                    <div>
                      <text><text style={{ fontWeight: 'bold' }}>Blok {val}</text> {CountUnit(val)} Unit</text>
                      <div className='progress mb-3 mt-3' >
                        <div
                          className='progress-bar bg-primary'
                          role='progressbar'
                          style={{ width: CountPercent(val) }}
                        >{CountPercent(val)}</div>
                      </div>
                    </div>
                  );
                })}

              </div>
              <div className='col-md-9'>
                <div
                  dangerouslySetInnerHTML={{ __html: siteMaps }}
                />
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div className={`card ${className}`} style={{
          minHeight: 600
        }}>
          <div className='card-body py-5 d-flex justify-content-center flex-column align-items-center'>
            <h1 className='mt-5'>Site Plan Belum di Upload</h1>
            <text className='text-muted mb-5'>Untuk memulai project anda perlu upload file site plan dengan format SVG </text>
            <input
              ref={inputRef}
              onChange={(e) => {
                handleChange(e)
              }}
              accept=".svg"
              type="file"
              style={{ display: "none" }}
            />
            <button
              onClick={() => inputRef.current?.click()}
              className="btn btn-primary m-5"
            >
              <span className="indicator-label">Add</span> <KTSVG path='/media/icon/add-filled.svg' className='svg-icon-3' />
            </button>
          </div>
        </div>
      );
    }
  }

  function renderTable() {
    if (dataID.length > 0) {
      return dataID.map((value, index) => {
        return <tr>
          <td>#{value}</td>
          <td style={{ textAlign: 'center' }}>B</td>
          <td style={{ textAlign: 'center' }}>1</td>
          <td style={{ textAlign: 'center' }}>
            <div className='card-toolbar'>
              <button
                type='button'
                style={{ background: '#D09C0A' }}
                className='btn btn-sm btn-icon btn-color-primary'
                data-kt-menu-trigger='click'
                data-kt-menu-placement='bottom-end'
                data-kt-menu-flip='top-end'
              >
                <KTSVG path='/media/icon/eye.svg' className='svg-icon-3' />
              </button>
              <div
                className='menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold w-200px'
                data-kt-menu='true'
              >
                <div className='separator mv-3 opacity-75'></div>
                <div className='menu-item' onClick={() => setShowModalUnit(true)}>
                  <text className='menu-link'>
                    Edit Unit
                  </text>
                </div>
                <div className='separator mv-3 opacity-75'></div>
                <div className='menu-item'>
                  <Link to='legalitas' className='menu-link'>
                    Detail Unit
                  </Link>
                </div>
              </div>
            </div>
          </td>
        </tr>

      })

    }
  }

  return (
    <div>
      {renderSiteMaps()}
      <ul
        className='nav nav-stretch nav-line-tabs fw-bold border-transparent flex-nowrap'
        role='tablist'
      >
        <li className='nav-item'>
          <a
            className={clsx(`nav-link cursor-pointer`, {
              active: tab === 'Unit',
            })}
            onClick={() => setTab('Unit')}
            role='tab'
          >
            Unit
          </a>
        </li>

        <li className='nav-item'>
          <a
            className={clsx(`nav-link cursor-pointer`, {
              active: tab === 'Variant',
            })}
            onClick={() => setTab('Variant')}
            role='tab'
          >
            Variant
          </a>
        </li>
      </ul>
      <div className='tab-content'>
        <div className={clsx(' mt-5 tab-pane', { active: tab === 'Unit' })}>

          <div className={`card mt-5 ${className}`}>
            <div className='card-body py-3'>
              <div className='table-responsive'>
                <table className='table table-row-bordered table-row-gray-100 align-middle gs-0 gy-3'>
                  <thead>
                    <tr className='fw-bolder text-muted'>
                      <th className='min-w-50px'>Unit ID</th>
                      <th className='min-w-140px'>Blok</th>
                      <th className='min-w-120px'>Nomor Unit</th>
                      <th className='min-w-80px'></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>#00123</td>
                      <td>B</td>
                      <td>1</td>
                      <td>
                        <div className='card-toolbar'>
                          <button
                            type='button'
                            style={{ background: '#D09C0A' }}
                            className='btn btn-sm btn-icon btn-color-primary'
                            data-kt-menu-trigger='click'
                            data-kt-menu-placement='bottom-end'
                            data-kt-menu-flip='top-end'
                          >
                            <KTSVG path='/media/icon/eye.svg' className='svg-icon-3' />
                          </button>
                          <div
                            className='menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold w-200px'
                            data-kt-menu='true'
                          >
                            <div className='separator mv-3 opacity-75'></div>
                            <div className='menu-item' onClick={() => setShowModalUnit(true)}>
                              <text className='menu-link'>
                                Edit Unit
                              </text>
                            </div>
                            <div className='separator mv-3 opacity-75'></div>
                            <div className='menu-item'>
                              <Link to='legalitas' className='menu-link'>
                                Detail Unit
                              </Link>
                            </div>
                          </div>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <div className={clsx('tab-pane mt-5', { active: tab === 'Variant' })}>
          <Breadcumbs
            data={breadcumbs}
            components={
              <button className='btn btn-primary w-100' onClick={() => setShowModal(true)}>
                <KTSVG path='/media/icon/home-add.svg' className='svg-icon-3' /><span className='indicator-label'>Add Variant</span>
              </button>}
          />
          <div className={`card mt-5 ${className}`}>
            <div className='card-body py-3'>
              <div className='table-responsive'>
                <table className='table table-row-bordered table-row-gray-100 align-middle gs-0 gy-3'>
                  <thead>
                    <tr className='fw-bolder text-muted'>
                      <th className='min-w-50px'>Variant ID</th>
                      <th className='min-w-140px'>Variant Name</th>
                      <th className='min-w-120px'>Description</th>
                      <th className='min-w-80px'></th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>#00123</td>
                      <td>Type 30 Cluster Mini</td>
                      <td>Foto Cluster pertama</td>
                      <td>
                        <div className='card-toolbar'>
                          <button
                            onClick={() => setShowModal(true)}
                            type='button'
                            style={{ background: '#D09C0A' }}
                            className='btn btn-sm btn-icon btn-color-primary'
                          >
                            <KTSVG path='/media/icon/edit-white.svg' className='svg-icon-3' />
                          </button>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Modal
        centered
        size="lg"
        show={showModal}
        onHide={() => setShowModal(false)}>
        <Modal.Body>
          <div className='card-title m-0'>
            <h3 className='fw-bolder m-0'>Add variant</h3>
            <p>Upload Foto unit serta foto pendukungnya</p>
          </div>

          <div className='row mb-6'>

            <div className='col-lg-12'>
              <div className='row mb-5'>
                <div className='col-lg-12 fv-row'>
                  <label className='col-lg-12 col-form-label required fw-bold fs-6'>Nama Variant</label>
                  <input
                    type='text'
                    className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                  />
                </div>
              </div>
              <div className='row mb-5'>
                <div className='col-lg-12 fv-row'>
                  <label className='col-lg-12 col-form-label required fw-bold fs-12'>Alamat Lengkap</label>
                  <textarea
                    className='form-control form-control-lg form-control-solid mb-3 mb-lg-0' rows={5}
                  />
                </div>
              </div>
              <div className='row'>
                <div className='col-lg-6 fv-row'>
                  <label className='col-lg-6 col-form-label required fw-bold fs-6'>Foto Employee</label>
                  <input
                    type='file'
                    className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                  />
                </div>
                <div className='col-lg-6 fv-row'>
                  <label className='col-lg-6 col-form-label required fw-bold fs-6'>Foto Employee</label>
                  <input
                    type='file'
                    className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                  />
                </div>
              </div>
              <div className='row mb-5'>
                <div className='col-lg-4 fv-row'>
                  <label className='col-lg-4 col-form-label required fw-bold fs-6'>Type</label>
                  <input
                    type='text'
                    className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                  />
                </div>
                <div className='col-lg-2 fv-row'>
                  <label className='col-lg-2 col-form-label required fw-bold fs-6'>LB</label>
                  <input
                    type='text'
                    className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                  />
                </div>
                <div className='col-lg-2 fv-row'>
                  <label className='col-lg-2 col-form-label required fw-bold fs-6'>LT</label>
                  <input
                    type='text'
                    className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                  />
                </div>
                <div className='col-lg-4 fv-row'>
                  <label className='col-lg-4 col-form-label required fw-bold fs-6'>Price</label>
                  <input
                    type='text'
                    className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                  />
                </div>
              </div>
              <div className="text-end mt-10">
                <button className='btn btn-white btn-active-light-primary me-5' onClick={() => setShowModal(false)}>Cancel</button>
                <button className='btn btn-primary' onClick={() => setShowModal(false)}>
                  <span className='indicator-label'>Add</span> <KTSVG path='/media/icon/add-filled.svg' className='svg-icon-3' />
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
      <Modal
        centered
        show={showModalUnit}
        onHide={() => setShowModalUnit(false)}>
        <Modal.Body>
          <div className='card-title m-0'>
            <h3 className='fw-bolder m-0'>Edit Unit</h3>
            <p>Edit details unit</p>
          </div>

          <div className='row mb-6'>
            <div className='col-lg-12'>
              <div className='row mb-5'>
                <div className='col-lg-12 fv-row'>
                  <label className='col-lg-12 col-form-label required fw-bold fs-6'>Nama Unit</label>
                  <input
                    type='text'
                    className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                  />
                </div>
                <div className='col-lg-12 fv-row'>
                  <label className='col-lg-12 col-form-label required fw-bold fs-6'>Variant</label>
                  <div className='col-lg-12 fv-row'>
                    <select
                      className='form-select form-select-solid form-select-lg fw-bold'>
                      <option value=''>Select Variant</option>
                    </select>
                  </div>
                </div>
                <div className='row'>
                  <div className='col-lg-10 fv-row'>
                    <label className='col-lg-10 col-form-label required fw-bold fs-6'>Alamat Unit</label>
                    <input
                      type='text'
                      className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                    />
                  </div>
                  <div className='col-lg-2 fv-row'>
                    <button className='btn btn-transparent mt-5'>
                      <img className='rounded mt-5' src={toAbsoluteUrl('/media/icon/pin-point.svg')} height="45" />
                    </button>
                  </div>
                </div>
                <div className='col-lg-12 fv-row'>
                  <label className='col-lg-12 col-form-label required fw-bold fs-6'>Blok</label>
                  <input
                    type='text'
                    className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                  />
                </div>
                <div className='col-lg-12 fv-row'>
                  <label className='col-lg-12 col-form-label required fw-bold fs-6'>Wing</label>
                  <input
                    type='text'
                    className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                  />
                </div>
                <div className='col-lg-12 fv-row'>
                  <label className='col-lg-12 col-form-label required fw-bold fs-6'>Price Up</label>
                  <input
                    type='text'
                    className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                  />
                </div>
              </div>
              <div className="text-end mt-10">
                <button className='btn btn-white btn-active-light-primary me-5' onClick={() => setShowModalUnit(false)}>Cancel</button>
                <button className='btn btn-primary' onClick={() => setShowModalUnit(false)}>
                  <span className='indicator-label'>Add</span> <KTSVG path='/media/icon/add-filled.svg' className='svg-icon-3' />
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  )
}

export { DetailSitePlan }
