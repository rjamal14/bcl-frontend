/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { KTSVG } from "../../../../_metronic/helpers";
import Breadcumbs from "../../../../_metronic/layout/components/Breadcumbs";
import Alert from "../../../../_metronic/layout/components/Alert";
import DataTable from "../../../../_metronic/partials/Table";
import { Modal } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { Dropdown } from "react-bootstrap";
import {
  getProject,
  addProject,
  deleteProject,
  updateProject,
} from "../../../../actions/project.actions";
const breadcumbs = [
  {
    title: "Super Admin",
    path: "",
  },
  {
    title: "List Project",
    path: "",
  },
];

const Lists = ({ className }) => {
  const dispatch = useDispatch();
  const project = useSelector((state) => state.project);
  const [type, setType] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [show, setShow] = useState(null);
  const [error, setError] = useState([]);
  const [id, setID] = useState(null);
  const [name, setName] = useState(null);
  const [address, setAddress] = useState(null);
  const [page, setPage] = useState(1);

  const list = project?.data?.data?.map((i, index) => {
    return {
      col1: "#" + i.id,
      col2: i.name,
      col3: i.address,
      col4: i,
    };
  });

  const data = React.useMemo(() => list, [project?.data?.data]);
  const columns = React.useMemo(
    () => [
      {
        Header: "Project ID",
        accessor: "col1",
        textAlign: "center",
      },
      {
        Header: "Project",
        accessor: "col2",
      },
      {
        Header: "Alamat",
        accessor: "col3",
        textAlign: "center",
      },
      {
        Header: "Aksi",
        accessor: "col4",
        Cell: ({ cell }) => (
          <div className="card-toolbar">
            <Dropdown className="dpw-setting">
              <Dropdown.Toggle
                style={{ background: "#D09C0A", paddingLeft: 10 }}
                className="btn btn-sm btn-icon btn-color-primary"
                variant="setting"
                id="dropdown-basic"
              >
                <KTSVG path="/media/icon/setting.svg" className="svg-icon-3" />
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item>
                  <div
                    className="menu-item"
                    onClick={() => handleEdit(cell.value)}
                  >
                    <text className="menu-link">Edit Project</text>
                  </div>
                </Dropdown.Item>
                <Dropdown.Item>
                  <div
                    className="menu-item"
                    onClick={() => handleDelete(cell.value.id)}
                  >
                    <text className="menu-link">Hapus Project</text>
                  </div>
                </Dropdown.Item>
                <Dropdown.Item>
                  <div className="menu-item">
                    <text className="menu-link">
                      <Link style={{ color: "#181c32" }} to="siteplan">
                        Siteplan
                      </Link>
                    </text>
                  </div>
                </Dropdown.Item>
                <Dropdown.Item>
                  <div className="menu-item">
                    <text className="menu-link">
                      <Link style={{ color: "#181c32" }} to="user-project">
                        User Project
                      </Link>
                    </text>
                  </div>
                </Dropdown.Item>
                <Dropdown.Item>
                  <div className="menu-item">
                    <text className="menu-link">
                      <Link
                        style={{ color: "#181c32" }}
                        to={"legalitas?id=" + cell.value.id}
                      >
                        Legalitas Project
                      </Link>
                    </text>
                  </div>
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>
        ),
        textAlign: "center",
      },
    ],
    [project?.data?.data]
  );

  function closeModal() {
    setShowModal(false);
    setName(null);
    setAddress(null);
    setError([]);
  }

  useEffect(() => {
    dispatch(getProject(page));
  }, [page]);

  function onSubmit() {
    if (type === "edit") {
      handleUpdate();
    } else {
      handleStore();
    }
  }

  function handleUpdate() {
    dispatch(
      updateProject(id, {
        name: name,
        address: address,
        description: "-",
      })
    )
      .then((data) => {
        if (data.status === "success") {
          dispatch(getProject(page));
          closeModal();
          setShow("success");
        }
      })
      .catch((error) => {
        setError(error.result);
      });
  }

  function handleStore() {
    dispatch(
      addProject({
        name: name,
        address: address,
        description: "-",
      })
    )
      .then((data) => {
        if (data.status === "success") {
          dispatch(getProject(page));
          closeModal();
          setShow("success");
        }
      })
      .catch((error) => {
        setError(error.result);
      });
  }

  function handleDelete(val) {
    dispatch(deleteProject(val)).then((data) => {
      if (data.status === "success") {
        dispatch(getProject(page));
        setShow("deleted");
      }
    });
  }

  function handleEdit(value) {
    console.log("value", value);
    setID(value.id);
    setAddress(value.address);
    setName(value.name);
    setType("edit");
    setShowModal(true);
  }

  return (
    <>
      <Alert
        show={show}
        setShow={(val) => {
          setShow(val);
        }}
      />
      <Breadcumbs
        data={breadcumbs}
        components={
          <button
            className="btn btn-primary w-100"
            onClick={() => setShowModal(true)}
          >
            <KTSVG path="/media/icon/home-add.svg" className="svg-icon-3" />
            <span className="indicator-label">Add Project</span>
          </button>
        }
      />

      {project?.data?.data && (
        <DataTable
          data={data}
          columns={columns}
          setPage={setPage}
          currentpage={project?.data?.currentPage}
          totalPage={parseInt(project?.data?.totalPages)}
        />
      )}

      <Modal centered show={showModal} onHide={() => closeModal()}>
        <Modal.Body>
          <div className="row">
            <div className="col-lg-12">
              <div className="row mb-5">
                <div className="col-lg-12 fv-row">
                  <label className="col-lg-12 col-form-label required fw-bold fs-6">
                    Nama Project
                  </label>
                  <input
                    value={name}
                    onChange={(e) => {
                      setName(e.target.value);
                    }}
                    type="text"
                    className="form-control form-control-lg form-control-solid mb-3 mb-lg-0"
                    placeholder="Nama Project"
                  />
                </div>
                <small id="passwordHelp" class="text-danger">
                  {error.filter((o) => o.param === "name").length > 0
                    ? "Nama Proyek " +
                      error.filter((o) => o.param === "name")[0].msg
                    : null}
                </small>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12">
              <div className="row mb-5">
                <div className="col-lg-12 fv-row">
                  <label className="col-lg-12 col-form-label required fw-bold fs-6">
                    Alamat
                  </label>
                  <input
                    value={address}
                    onChange={(e) => {
                      setAddress(e.target.value);
                    }}
                    type="text"
                    className="form-control form-control-lg form-control-solid mb-3 mb-lg-0"
                    placeholder="Isi Alamat Lengkap Project"
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12">
              <div className="text-end mt-10">
                <button
                  type="button"
                  className="btn btn-primary me-5"
                  onClick={() => onSubmit()}
                >
                  Simpan
                </button>
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                  onClick={() => closeModal()}
                >
                  Batalkan
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default Lists;
