/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useState, useEffect } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { KTSVG } from '../../../../_metronic/helpers/components/KTSVG'

type Props = {
  className: string
}

const Siteplan: React.FC<Props> = ({ className }) => {
  const navigate = useNavigate()
  const [dataID, setDataID] = useState<string[]>([])
  const [siteMaps, setSiteMaps] = useState('')
  const [Blok, setBlok] = useState<string[]>([])
  const [Count, setCount] = useState<number>(0)
  const inputRef = React.useRef<HTMLInputElement>(null)


  function someHandler(value: string) {
    var id = value;
    var svgState = document.querySelector("#" + id);
    if (svgState !== null) {
      svgState.classList.add("hover");
    }
  }

  useEffect(() => {
    console.log('Blok', Blok);
  }, [Blok, Count])

  function removeAllHover() {
    var svgStates = document.querySelectorAll("#states > *");
    svgStates.forEach(function (el) {
      el.classList.remove("hover");
    });
  }

  function handleChange(e: React.ChangeEvent<HTMLInputElement>) {
    const data = e.target.files;
    if (data !== null && data.length > 0) {
      const fileReader = new FileReader();
      fileReader.readAsText(data[0], "UTF-8");
      fileReader.onload = (res) => {
        var patern = res !== null ? res.target?.result : "";
        if (patern !== null && patern !== undefined) {
          setSiteMaps(String(patern));
          var temp = String(patern).split("<");
          let SetID: string[] = [];
          temp.map((value, index) => {
            if (temp[index].substring(0, 7) === "polygon") {
              let a = String(value).split("fill")[0];
              SetID.push(String(a).split('"')[1])
            }
          });
          setDataID(SetID)
        }
      }
    }
  }

  function CountPercent(id: string) {
    const total = dataID.length;
    const bagian = Blok.filter((o) => o === id).length;
    return bagian / total * 100 + '%';
  }

  function CountUnit(id: string) {
    return Blok.filter((o) => o === id).length;
  }

  function renderSiteMaps() {
    if (dataID.length > 0) {

      const unique = (value: any, index: any, self: string | any[]) => {
        return self.indexOf(value) === index
      }

      const uniqueBlok = Blok.filter(unique)

      console.log(uniqueBlok)

      return (
        <div className={`card ${className}`}>
          <div className='card-body py-3'>
            <div className='row'>
              <div className='col-md-3'>
                <h4 className='mt-5 mb-5'>Properties Map Location</h4>

                {uniqueBlok.map((val) => {
                  return (
                    <div>
                      <text><text style={{ fontWeight: 'bold' }}>Blok {val}</text> {CountUnit(val)} Unit</text>
                      <div className='progress mb-3 mt-3' >
                        <div
                          className='progress-bar bg-primary'
                          role='progressbar'
                          style={{ width: CountPercent(val) }}
                        >{CountPercent(val)}</div>
                      </div>
                    </div>
                  );
                })}

              </div>
              <div className='col-md-9'>
                <div
                  dangerouslySetInnerHTML={{ __html: siteMaps }}
                />
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div className={`card ${className}`} style={{
          minHeight: 600
        }}>
          <div className='card-body py-5 d-flex justify-content-center flex-column align-items-center'>
            <h1 className='mt-5'>Site Plan Belum di Upload</h1>
            <text className='text-muted mb-5'>Untuk memulai project anda perlu upload file site plan dengan format SVG </text>
            <input
              ref={inputRef}
              onChange={(e) => {
                handleChange(e)
              }}
              accept=".svg"
              type="file"
              style={{ display: "none" }}
            />
            <button
              onClick={() => inputRef.current?.click()}
              className="btn btn-primary m-5"
            >
              <span className="indicator-label">Add</span> <KTSVG path='/media/icon/add-filled.svg' className='svg-icon-3' />
            </button>
          </div>
        </div>
      );
    }
  }

  function renderTable() {
    if (dataID.length > 0) {
      return dataID.map((value, index) => {
        return <table className='table table-row-bordered table-row-gray-100 align-middle gs-0 gy-3'>
          <thead>
            <tr className='fw-bolder text-muted'>
              <th>Unit ID</th>
              <th>Blok</th>
              <th>Nomor Unit</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>#{value}</td>
              <td>
                <input
                  onFocus={() => {
                    someHandler(value);
                  }}
                  onChange={(e) => {
                    let dataBlok = Blok || [];
                    dataBlok[index] = e.target.value;
                    setBlok(dataBlok)
                    setCount(Count + 1)
                  }}
                  onBlur={() => {
                    removeAllHover()
                  }}
                  type='text'
                  className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                />
              </td>
              <td>
                <input
                  onFocus={() => {
                    someHandler(value);
                  }}
                  onBlur={() => {
                    removeAllHover()
                  }}
                  type='text'
                  className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                />
              </td>
            </tr>
          </tbody>
        </table>
      })

    }
  }

  return (
    <div>
      {renderSiteMaps()}
      {dataID.length > 0 && <div className={`card ${className}`}>
        <div className='card-body py-3'>
          <div className='table-responsive'>
            {renderTable()}
          </div>
            <div className="row mb-5">
              <div className='col-lg-12 mb-5'>
                <div className='text-end mt-10'>
                  <button type="button" className="btn btn-primary me-5" onClick={() => { }}>Simpan</button>
                  <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={() => { }}>Batalkan</button>
                </div>
              </div>
            </div>
        </div>
      </div>}
    </div>
  )
}

export { Siteplan }
