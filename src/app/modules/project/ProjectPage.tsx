import { Navigate, Route, Routes, Outlet, useNavigate } from 'react-router-dom'
import { PageLink, PageTitle } from '../../../_metronic/layout/core'
import { Link } from 'react-router-dom'
import  Lists from './components/Lists'
import Breadcumbs from '../../../_metronic/layout/components/Breadcumbs'
import { KTSVG } from '../../../_metronic/helpers'
import { Siteplan } from './components/Siteplan'
import { DetailSitePlan } from './components/DetailSitePlan'


const breadcumbsSitePlan = [
  {
    title: 'Super Admin',
    path: '',
  },
  {
    title: 'List Project',
    path: '',
  },
  {
    title: 'Siteplan',
    path: '',
  },
]

const ProjectPage = () => {
  const navigate = useNavigate()

  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='/'
          element={
            <>
              <Lists className='mb-5 mb-xl-12' />
            </>
          }
        />
        {/* <Route index element={<Navigate to='/crafted/widgets/lists' />} /> */}
        <Route
          path="/siteplan"
          element={
            <>
              <Breadcumbs
                data={breadcumbsSitePlan}
                components={
                  <Link
                    to="/superadmin/project"
                    className="btn btn-primary w-100"
                  >
                    <KTSVG path='/media/icon/arrow-back.svg' className='svg-icon-3' /> <span className="indicator-label">Back</span>
                  </Link>
                }
              />
              <Siteplan className="mb-5 mb-xl-12" />
            </>
          }
        />
        <Route
          path="/siteplan/detail"
          element={
            <>
              <Breadcumbs
                data={breadcumbsSitePlan}
                components={
                  <Link
                    to="/superadmin/project"
                    className="btn btn-primary w-100"
                  >
                    <KTSVG path='/media/icon/arrow-back.svg' className='svg-icon-3' /> <span className="indicator-label">Back</span>
                  </Link>
                }
              />
              <DetailSitePlan className="mb-5 mb-xl-12" />
            </>
          }
        />
      </Route>
    </Routes>
  )
}

export default ProjectPage
