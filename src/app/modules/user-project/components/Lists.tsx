/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useState } from 'react'
import { KTSVG } from '../../../../_metronic/helpers'
import { toAbsoluteUrl } from '../../../../_metronic/helpers'
import { Link } from 'react-router-dom'
import Breadcumbs from '../../../../_metronic/layout/components/Breadcumbs'
import BackButton from '../../../../_metronic/layout/components/BackButton'
import { Modal } from 'react-bootstrap'

const breadcumbs = [
  {
    title: 'Super Admin',
    path: '',
  },
  {
    title: 'List Project',
    path: '',
  },
  {
    title: 'User Project',
    path: '',
  },
]

type Props = {
  className: string
}

const Lists: React.FC<Props> = ({ className }) => {
  const [showModal, setShowModal] = useState(false)

  return (
    <>
      <Breadcumbs
        data={breadcumbs}
        components={
          <div className="d-flex row-column">
            <button className='btn btn-primary h-100 me-3' onClick={() => setShowModal(true)}>
              <KTSVG path='/media/icon/home-add.svg' className='svg-icon-3 ' /><span className='indicator-label'>Assign User</span>
            </button>
            <BackButton />
          </div>}
      />

      <div className={`card ${className}`}>
        <div className='card-body py-3'>
          <div className='table-responsive'>
            <table className='table table-row-bordered table-row-gray-100 align-middle gs-0 gy-3'>
              <thead>
                <tr className='fw-bolder text-muted'> 
                  <th className='min-w-50px'>Photo</th>
                  <th className='min-w-140px'>ID</th>
                  <th className='min-w-120px'>Name</th>
                  <th className='min-w-120px'>Position</th>
                  <th className='min-w-120px'>Email</th>
                  <th className='min-w-120px'>Phone</th>
                  <th className='min-w-120px'>Status</th>
                  <th className='min-w-80px'>Action</th>
                </tr>
              </thead>
              <tbody>
                <tr>  
                  <td>
                    <a href='#' className='text-dark fw-bolder text-hover-primary fs-6'>
                      <img className='rounded' src={toAbsoluteUrl('/media/avatars/300-1.jpg')} width="48px" />
                    </a>
                  </td>
                  <td>#00123</td>
                  <td>Jamal Solehin</td>
                  <td>Marketing</td>
                  <td>open@gmail.com</td>
                  <td>+6281234567890</td>
                  <td>Aktif</td>
                  <td>  
                    <div className='card-toolbar'>
                      <button
                        type='button'
                        style={{ background: '#D09C0A' }}
                        className='btn btn-sm btn-icon btn-color-primary'
                        data-kt-menu-trigger='click'
                        data-kt-menu-placement='bottom-end'
                        data-kt-menu-flip='top-end'
                      >
                        <KTSVG path='/media/icon/setting.svg' className='svg-icon-3' />
                      </button>
                      {/* <div
                        className='menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold w-200px'
                        data-kt-menu='true'
                      >
                        <div className='separator mv-3 opacity-75'></div>
                        <div className='menu-item'>
                          <a href='#' className='menu-link'>
                            Siteplan
                          </a>
                        </div>
                        <div className='separator mv-3 opacity-75'></div>
                        <div className='menu-item'>
                          <a href='#' className='menu-link'>
                            User Project
                          </a>
                        </div>
                        <div className='separator mv-3 opacity-75'></div>
                        <div className='menu-item'>
                          <a href='#' className='menu-link'>
                            Legalitas Project
                          </a>
                        </div>
                      </div> */}
                    </div>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div> 

      <Modal
        size='lg'
        centered
        show={showModal}
        onHide={() => setShowModal(false)}>
        <Modal.Body>
          <div className='row'>
            <div className='col-lg-12'>
              <div className='row mb-5'>
                <div className='col-lg-12 fv-row'>
                  <input
                    type='text'
                    className='form-control form-control-lg form-control-solid mb-3 mb-lg-0'
                    placeholder='Search Employee'
                  />
                </div>

                <div className='table-responsive'>
                  <table className='table table-row-bordered table-row-gray-100 align-middle gs-0 gy-3'>
                    <thead>
                      <tr className='fw-bolder text-muted'>
                        <th className='min-w-50px'>Photo</th>
                        <th className='min-w-140px'>ID</th>
                        <th className='min-w-120px'>Name</th>
                        <th className='min-w-120px'>Position</th>
                        <th className='min-w-120px'>Email</th>
                        <th className='min-w-120px'>Phone</th>
                        <th className='min-w-120px'>Status</th>
                        <th className='min-w-80px'>Assign</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          <a href='#' className='text-dark fw-bolder text-hover-primary fs-6'>
                            <img className='rounded' src={toAbsoluteUrl('/media/avatars/300-1.jpg')} width="48px" />
                          </a>
                        </td>
                        <td>#00123</td>
                        <td>Jamal Solehin</td>
                        <td>Marketing</td>
                        <td>open@gmail.com</td>
                        <td>+6281234567890</td>
                        <td>Aktif</td>
                        <td><input type='checkbox'/></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className='col-lg-12'>
              <div className='text-end mt-10'>
                <button type="button" className="btn btn-primary me-5" onClick={() => setShowModal(false)}>Simpan</button>
                <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={() => setShowModal(false)}>Batalkan</button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  )
}

export { Lists }
