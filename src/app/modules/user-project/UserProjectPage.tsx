import {Navigate, Route, Routes, Outlet, useNavigate} from 'react-router-dom'
import {PageLink, PageTitle} from '../../../_metronic/layout/core'
import { Link } from 'react-router-dom'
import {Lists} from './components/Lists'
import Breadcumbs from '../../../_metronic/layout/components/Breadcumbs'
import { KTSVG } from '../../../_metronic/helpers'

const UserProject = () => {
  const navigate = useNavigate()

  return (
    <Routes>
      <Route element={<Outlet />}>
        <Route
          path='/'
          element={
            <>
              <Lists className='mb-5 mb-xl-12' />
            </>
          }
        />
        {/* <Route index element={<Navigate to='/crafted/widgets/lists' />} /> */}
      </Route>
    </Routes>
  )
}

export default UserProject
