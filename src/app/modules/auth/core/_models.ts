export interface AuthModel {
  result: string,
  status: string,
  message: string,
}

export interface UserAddressModel {
  addressLine: string
  city: string
  state: string
  postCode: string
}

export interface UserCommunicationModel {
  email: boolean
  sms: boolean
  phone: boolean
}

export interface UserEmailSettingsModel {
  emailNotification?: boolean
  sendCopyToPersonalEmail?: boolean
  activityRelatesEmail?: {
    youHaveNewNotifications?: boolean
    youAreSentADirectMessage?: boolean
    someoneAddsYouAsAsAConnection?: boolean
    uponNewOrder?: boolean
    newMembershipApproval?: boolean
    memberRegistration?: boolean
  }
  updatesFromKeenthemes?: {
    newsAboutKeenthemesProductsAndFeatureUpdates?: boolean
    tipsOnGettingMoreOutOfKeen?: boolean
    thingsYouMissedSindeYouLastLoggedIntoKeen?: boolean
    newsAboutStartOnPartnerProductsAndOtherServices?: boolean
    tipsOnStartBusinessProducts?: boolean
  }
}

export interface UserSocialNetworksModel {
  linkedIn: string
  facebook: string
  twitter: string
  instagram: string
}

export interface UserModel {
  "status": "success",
  "message": "Successfully",
  "result": {
      "exp": 1657167049,
      "data": {
          "type": "user",
          "id": 2,
          "user": {
              "id": 2,
              "username": "darulfajarr@gmail.com",
              "password": "ac440e112f7f0bd4c4aec01becd612b4853027338ed2e2d8d4ea431239223dfdad94f148c739f0e97c255deac060a514acc176d21ef97876bf285f4ddd443a58d0b2b72ba49874bfb083849859d7f444fa40de428d9e636c2c2c6e816713b630d1c36978802feb53",
              "role": "superadmin",
              "createdAt": "2022-07-05T08:31:08.000Z",
              "updatedAt": "2022-07-05T08:31:08.000Z",
              "deletedAt": null,
              "profile": {
                  "id": 2,
                  "user_id": 2,
                  "email": "darulfajarr@gmail.com",
                  "phone": null,
                  "photo": null,
                  "first_name": "Darul",
                  "last_name": "Fajar Ramadhan",
                  "full_name": "Darul Fajar Ramadhan",
                  "sex": null,
                  "address": "-",
                  "province_id": null,
                  "city_id": null,
                  "postal_code": null,
                  "createdAt": "2022-07-05T08:31:08.000Z",
                  "updatedAt": "2022-07-05T08:31:08.000Z",
                  "deletedAt": null
              }
          }
      },
      "iat": 1657080649
  }
}