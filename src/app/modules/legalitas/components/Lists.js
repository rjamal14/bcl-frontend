/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { useEffect, useRef, useState } from 'react'
import { KTSVG } from '../../../../_metronic/helpers'
import Breadcumbs from '../../../../_metronic/layout/components/Breadcumbs'
import BackButton from '../../../../_metronic/layout/components/BackButton'
import { Modal } from 'react-bootstrap'
import { useDispatch, useSelector } from "react-redux";
import {
  getProjectLegal,
  addProjectLegal,
  deleteProjectLegal,
  updateProjectLegal
} from "../../../../actions/project-legal.actions";
import { dateIndo } from '../../../../_metronic/helpers/dateIndo'
import DataTable from "../../../../_metronic/partials/Table";
import { Dropdown } from "react-bootstrap";

const breadcumbs = [
  {
    title: 'Super Admin',
    path: '',
  },
  {
    title: 'List Project',
    path: '',
  },
  {
    title: 'Legalitas',
    path: '',
  },
]

const Lists = ({ className }) => {
  const dispatch = useDispatch();
  const projectLegal = useSelector((state) => state.projectLegal);
  const [showModalChooseFile, setShowModalChooseFile] = useState(false)
  const [showModalFile, setShowModalFile] = useState(false)
  const [selectedFile, setSelectedFile] = useState(null)
  const [fileName, setFileName] = useState('')
  const [fileType, setFileType] = useState('')
  const [fileDescription, setFileDescription] = useState('')
  const [editFileName, setEditFileName] = useState(false)
  const [editFileType, setEditFileType] = useState(false)
  const [editFileDescription, setEditFileDescription] = useState(false)
  const fileInput = useRef()
  const [type, setType] = useState(null);
  const [show, setShow] = useState(null);
  const [error, setError] = useState([]);
  const [id, setID] = useState(null);
  const [page, setPage] = useState(1);

  const selectFile = () => {
    fileInput.current.click();
  }


  useEffect(() => {
    dispatch(getProjectLegal(page, window.location.href.split("?id=")[1]));
  }, [page]);



  const list = projectLegal?.data?.data?.map((i, index) => {
    return {
      col1: i.name,
      col2: '-',
      col3: i.description,
      col4: dateIndo(i.expired),
      col5: i,
    };
  });

  const data = React.useMemo(() => list, [projectLegal?.data?.data]);
  const columns = React.useMemo(
    () => [
      {
        Header: "Nama File",
        accessor: "col1",
        textAlign: "center",
      },
      {
        Header: "Jenis",
        accessor: "col2",
      },
      {
        Header: "Keterangan",
        accessor: "col3",
        textAlign: "center",
      },
      {
        Header: "Expired",
        accessor: "col4",
        textAlign: "center",
      },
      {
        Header: "Aksi",
        accessor: "col5",
        Cell: ({ cell }) => (
          <div className="card-toolbar">
            <Dropdown className="dpw-setting">
              <Dropdown.Toggle
                style={{ background: "#D09C0A", paddingLeft: 10 }}
                className="btn btn-sm btn-icon btn-color-primary"
                variant="setting"
                id="dropdown-basic"
              >
                <KTSVG path="/media/icon/setting.svg" className="svg-icon-3" />
              </Dropdown.Toggle>
              <Dropdown.Menu>
                <Dropdown.Item>
                  <div
                    className="menu-item"
                    onClick={() => handleEdit(cell.value)}
                  >
                    <text className="menu-link">Download Legalitas</text>
                  </div>
                </Dropdown.Item>
                <Dropdown.Item>
                  <div
                    className="menu-item"
                    onClick={() => handleEdit(cell.value)}
                  >
                    <text className="menu-link">Edit Legalitas</text>
                  </div>
                </Dropdown.Item>
                <Dropdown.Item>
                  <div
                    className="menu-item"
                    onClick={() => handleDelete(cell.value.id)}
                  >
                    <text className="menu-link">Hapus Legalitas</text>
                  </div>
                </Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
          </div>
        ),
        textAlign: "center",
      },
    ],
    [projectLegal?.data?.data]
  );


  function onSubmit() {
    if (type === 'edit') {
      // handleUpdate()
    } else {
      handleStore()
    }
  }

  // function handleUpdate() {
  //   dispatch(
  //     updateProjectLegal(id,{
  //       project_id:1,
  //       name: fileName,
  //       description: fileDescription,

  //     })
  //   )
  //     .then((data) => {
  //       if (data.status === "success") {
  //         dispatch(getProject());
  //         closeModal();
  //         setShow("success");
  //       }
  //     })
  //     .catch((error) => {
  //       setError(error.result);
  //     });
  // }

  function handleStore() {
    const formData = new FormData();
    formData.append('file', selectedFile)
    formData.append('project_id', window.location.href.split("?id=")[1])
    formData.append('name', fileName)
    formData.append('description', fileDescription)

    dispatch(
      addProjectLegal(formData)
    ).then((data) => {
      if (data.status === "success") {
        dispatch(getProjectLegal(page, window.location.href.split("?id=")[1]));
        closeModal();
        setShow("success");
      }
    }).catch((error) => {
      setError(error.result);
    });
  }

  function handleDelete(val) {
    dispatch(
      deleteProjectLegal(val)
    ).then((data) => {
      if (data.status === "success") {
        dispatch(getProjectLegal(page, window.location.href.split("?id=")[1]));
        setShow("deleted");
      }
    });
  }

  function handleEdit(value) {
    // setID(value.id)
    // setAddress(value.address)
    // setName(value.name)
    // setType('edit')
    // setShowModal(true)
  }

  function closeModal() {
    setShowModalChooseFile(false);
    setShowModalFile(false);
    setFileName('');
    setFileType('');
    setFileDescription('');
    setError([]);
  }


  useEffect(() => {
    if (selectedFile != null) {
      console.log(selectedFile.name)
      setShowModalChooseFile(false)
      setShowModalFile(true)
      setFileName(selectedFile?.name)
    }
  }, [selectedFile])

  return (
    <>
      <Breadcumbs
        data={breadcumbs}
        components={
          <div className="d-flex row-column">
            <button className='btn btn-primary h-100 me-3' onClick={() => setShowModalChooseFile(true)}>
              <KTSVG path='/media/icon/home-add.svg' className='svg-icon-3 ' /><span className='indicator-label'>Upload Legal</span>
            </button>
            <BackButton />
          </div>}
      />

      {projectLegal?.data?.data && (
        <DataTable
          data={data}
          columns={columns}
          setPage={setPage}
          currentpage={projectLegal?.data?.currentPage}
          totalPage={parseInt(projectLegal?.data?.totalPages)}
        />
      )}
      <Modal
        size='lg'
        centered
        show={showModalChooseFile}
        onHide={() => setShowModalChooseFile(false)}>
        <Modal.Body>
          <div className='row py-15'>
            <div className='col-lg-12 fv-row text-center'>
              <h3>Upload Dokumen Legalitas</h3>
              <span style={{ color: '#BABABA' }}>Untuk memulai project anda perlu upload file site plan dengan format SVG </span>
              <input
                className='d-none'
                accept='.png,.jpg,.jpeg,.pdf'
                type="file"
                ref={fileInput}
                onChange={(e) => {
                  console.log(e.target.files)
                  if (e.target.files != null) {
                    setSelectedFile(e.target.files[0])
                  }
                }}
              />
            </div>
            <div className='col-lg-12 fv-row text-center mt-10'>
              <button className='btn btn-primary h-100 me-3' onClick={selectFile}>
                <span className='indicator-label me-2'>Add</span><KTSVG path='/media/icon/add-filled.svg' className='svg-icon-3 ' />
              </button>
            </div>
          </div>
        </Modal.Body>
      </Modal>

      <Modal
        size='lg'
        centered
        show={showModalFile}
        onHide={() => setShowModalFile(false)}>
        <Modal.Body>
          <div className='row py-15'>
            <div className='col-lg-12 fv-row text-center'>
              <div style={{ backgroundColor: '#D9D9D9', borderRadius: 5, padding: 50 }}>

              </div>
              <div className='table-responsive mt-5'>
                <table className='table table-row-bordered table-row-gray-100 align-middle gs-0 gy-3'>
                  <thead>
                    <tr className='fw-bolder text-muted'>
                      <th className='min-w-50px'>Nama File</th>
                      <th className='min-w-140px'>Jenis</th>
                      <th className='min-w-120px'>Keterangan</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <div className='d-flex align-items-center'>
                          <input
                            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0 me-3'
                            type='text'
                            value={fileName}
                            onChange={(e) => setFileName(e.target.value)}
                            disabled={!editFileName}
                          />
                          <a onClick={() => setEditFileName(!editFileName)}>{!editFileName ? <KTSVG path='/media/icon/icon-pencil.svg' className='svg-icon-3' /> : <KTSVG path='/media/icon/check.svg' className='svg-icon-3' />}</a>
                        </div>
                        <small style={{ textAlign: 'left' }} id="passwordHelp" class="text-danger">
                          {error.filter((o) => o.param === "name").length > 0
                            ? "Nama File " +
                            error.filter((o) => o.param === "name")[0].msg
                            : null}
                        </small>
                      </td>
                      <td>
                        <div className='d-flex align-items-center'>
                          <input
                            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0 me-3'
                            type='text'
                            value={fileType}
                            disabled
                          // onChange={(e) => setFileType(e.target.value)}
                          // disabled={!editFileType}
                          />
                          {/* <a onClick={() => setEditFileType(!editFileType)}>{!editFileType ? <KTSVG path='/media/icon/icon-pencil.svg' className='svg-icon-3' /> : <KTSVG path='/media/icon/check.svg' className='svg-icon-3' />}</a> */}
                        </div>
                      </td>
                      <td>
                        <div className='d-flex align-items-center'>
                          <input
                            className='form-control form-control-lg form-control-solid mb-3 mb-lg-0 me-3'
                            type='text'
                            value={fileDescription}
                            onChange={(e) => setFileDescription(e.target.value)}
                            disabled={!editFileDescription}
                          />
                          <a onClick={() => setEditFileDescription(!editFileDescription)}>{!editFileDescription ? <KTSVG path='/media/icon/icon-pencil.svg' className='svg-icon-3' /> : <KTSVG path='/media/icon/check.svg' className='svg-icon-3' />}</a>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div className='col-lg-12 fv-row text-end mt-10'>
              <button className='btn btn-primary me-3' onClick={() => onSubmit()}>
                <span className='indicator-label me-2'>Save</span>
              </button>
              <button className='btn btn-secondary me-5' onClick={() => setShowModalFile(false)}>
                <span className='indicator-label me-2'>Batal</span>
              </button>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  )
}

export default Lists;