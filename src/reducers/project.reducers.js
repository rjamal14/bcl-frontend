/* eslint-disable default-case */
/* eslint-disable import/no-anonymous-default-export */
import { ProjectConstants } from "../actions/constants"

const initState = {
  loading: true,
  data: [],
}

export default (state = initState, action) => {
  switch (action.type) {
    case ProjectConstants.FETCH_PROJECT_REQUEST:
      state = {
        ...state,
        loading: true
      }
      break;
    case ProjectConstants.FETCH_PROJECT_SUCCESS:
      state = {
        ...state,
        loading: false,
        data: action.payload.program,
      }
      break;
    case ProjectConstants.FETCH_PROJECT_FAILURE:
      state = {
        ...state,
        loading: false,
        error: action.payload.error
      }
      break;

    case ProjectConstants.FETCH_PROJECT_POST_REQUEST:
      state = {
        ...state,
        loading: true
      }
      break;
    case ProjectConstants.FETCH_PROJECT_POST_SUCCESS:
      state = {
        ...state,
        loading: false,
        message: action.payload.message
      }
      break;
    case ProjectConstants.FETCH_PROJECT_POST_FAILURE:
      state = {
        ...state,
        loading: false,
        error: action.payload.error
      }
      break;
  }

  return state;
}