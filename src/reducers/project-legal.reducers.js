/* eslint-disable default-case */
/* eslint-disable import/no-anonymous-default-export */
import { ProjectLegalConstants } from "../actions/constants"

const initState = {
  loading: true,
  data: [],
}

export default (state = initState, action) => {
  switch (action.type) {
    case ProjectLegalConstants.FETCH_PROJECT_LEGAL_REQUEST:
      state = {
        ...state,
        loading: true
      }
      break;
    case ProjectLegalConstants.FETCH_PROJECT_LEGAL_SUCCESS:
      state = {
        ...state,
        loading: false,
        data: action.payload.program,
      }
      break;
    case ProjectLegalConstants.FETCH_PROJECT_LEGAL_FAILURE:
      state = {
        ...state,
        loading: false,
        error: action.payload.error
      }
      break;

    case ProjectLegalConstants.FETCH_PROJECT_LEGAL_POST_REQUEST:
      state = {
        ...state,
        loading: true
      }
      break;
    case ProjectLegalConstants.FETCH_PROJECT_LEGAL_POST_SUCCESS:
      state = {
        ...state,
        loading: false,
        message: action.payload.message
      }
      break;
    case ProjectLegalConstants.FETCH_PROJECT_LEGAL_POST_FAILURE:
      state = {
        ...state,
        loading: false,
        error: action.payload.error
      }
      break;
  }

  return state;
}