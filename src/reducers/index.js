import { combineReducers } from 'redux';
import projectReducers from './project.reducers';
import projectLegalReducers from './project-legal.reducers';

const rootReducer = combineReducers({
    project:projectReducers,
    projectLegal:projectLegalReducers,
});

export default rootReducer;