import { ProjectConstants } from "./constants";
import axios from "../helpers/axios";

export const getProject = (page) => {
    return async (dispatch) => {
        dispatch({ type: ProjectConstants.FETCH_PROJECT_REQUEST })
        try {
            await axios.get(`project?page=${page}`)
                .then(response => {
                    return response.data
                })
                .then(data => {
                    if (data.status === "success") {
                        const program = data.result ?? []
                        dispatch({
                            type: ProjectConstants.FETCH_PROJECT_SUCCESS,
                            payload: { program }
                        })
                    }
                })
                .catch(error => {
                    console.log('error', error);
                    dispatch({
                        type: ProjectConstants.FETCH_PROJECT_FAILURE,
                        payload: { error: error }
                    })
                })
        } catch (err) {
            console.log(err);
            dispatch({
                type: ProjectConstants.FETCH_PROJECT_FAILURE,
                payload: { error: "Terjadi kesalahan mohon ulangi beberapa saat lagi" }
            })
        }
    }
}


export const addProject = (payload) => async (dispatch) => {
    return new Promise(async (resolve, rejected) => {
        dispatch({ type: ProjectConstants.FETCH_PROJECT_POST_REQUEST })
        try {
            await axios.post(`project`, payload)
                .then(response => {
                    return response.data
                })
                .then(data => {
                    console.log('data', data);
                    if (data.status === "success") {
                        resolve(data)
                    }else{
                        rejected(data)
                    }
                })
                .catch(error => {
                    resolve(error)
                })
        } catch (err) {
            rejected(err)
            dispatch({
                type: ProjectConstants.FETCH_PROJECT_POST_FAILURE,
                payload: { error: "Terjadi kesalahan mohon ulangi beberapa saat lagi" }
            })
            rejected("Terjadi kesalahan mohon ulangi beberapa saat lagi")
        }
    })
}


export const deleteProject = (id) => async (dispatch) => {
    return new Promise(async (resolve, rejected) => {
        dispatch({ type: ProjectConstants.FETCH_PROJECT_POST_REQUEST })
        try {
            await axios.delete(`project/${id}`)
                .then(response => {
                    return response.data
                })
                .then(data => {
                    console.log('data', data);
                    if (data.status === "success") {
                        resolve(data)
                    }else{
                        rejected(data)
                    }
                })
                .catch(error => {
                    resolve(error)
                })
        } catch (err) {
            rejected(err)
            dispatch({
                type: ProjectConstants.FETCH_PROJECT_POST_FAILURE,
                payload: { error: "Terjadi kesalahan mohon ulangi beberapa saat lagi" }
            })
            rejected("Terjadi kesalahan mohon ulangi beberapa saat lagi")
        }
    })
}

export const updateProject = (id,payload) => async (dispatch) => {
    return new Promise(async (resolve, rejected) => {
        dispatch({ type: ProjectConstants.FETCH_PROJECT_POST_REQUEST })
        try {
            await axios.patch(`project/${id}`,payload)
                .then(response => {
                    return response.data
                })
                .then(data => {
                    console.log('data', data);
                    if (data.status === "success") {
                        resolve(data)
                    }else{
                        rejected(data)
                    }
                })
                .catch(error => {
                    resolve(error)
                })
        } catch (err) {
            rejected(err)
            dispatch({
                type: ProjectConstants.FETCH_PROJECT_POST_FAILURE,
                payload: { error: "Terjadi kesalahan mohon ulangi beberapa saat lagi" }
            })
            rejected("Terjadi kesalahan mohon ulangi beberapa saat lagi")
        }
    })
}

