import { ProjectLegalConstants } from "./constants";
import axios from "../helpers/axios";

export const getProjectLegal = (page,id) => {
    return async (dispatch) => {
        dispatch({ type: ProjectLegalConstants.FETCH_PROJECT_LEGAL_REQUEST })
        try {
            await axios.get(`project-legal?page=${page}&project_id=${id}`)
                .then(response => {
                    return response.data
                })
                .then(data => {
                    if (data.status === "success") {
                        const program = data.result ?? []
                        dispatch({
                            type: ProjectLegalConstants.FETCH_PROJECT_LEGAL_SUCCESS,
                            payload: { program }
                        })
                    }
                })
                .catch(error => {
                    console.log('error', error);
                    dispatch({
                        type: ProjectLegalConstants.FETCH_PROJECT_LEGAL_FAILURE,
                        payload: { error: error }
                    })
                })
        } catch (err) {
            console.log(err);
            dispatch({
                type: ProjectLegalConstants.FETCH_PROJECT_LEGAL_FAILURE,
                payload: { error: "Terjadi kesalahan mohon ulangi beberapa saat lagi" }
            })
        }
    }
}


export const addProjectLegal = (payload) => async (dispatch) => {
    return new Promise(async (resolve, rejected) => {
        dispatch({ type: ProjectLegalConstants.FETCH_PROJECT_LEGAL_POST_REQUEST })
        try {
            const config = {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            }
            await axios.post(`project-legal`, payload, config)
                .then(response => {
                    return response.data
                })
                .then(data => {
                    console.log('data', data);
                    if (data.status === "success") {
                        resolve(data)
                    }else{
                        rejected(data)
                    }
                })
                .catch(error => {
                    resolve(error)
                })
        } catch (err) {
            rejected(err)
            dispatch({
                type: ProjectLegalConstants.FETCH_PROJECT_LEGAL_POST_FAILURE,
                payload: { error: "Terjadi kesalahan mohon ulangi beberapa saat lagi" }
            })
            rejected("Terjadi kesalahan mohon ulangi beberapa saat lagi")
        }
    })
}


export const deleteProjectLegal = (id) => async (dispatch) => {
    return new Promise(async (resolve, rejected) => {
        dispatch({ type: ProjectLegalConstants.FETCH_PROJECT_LEGAL_POST_REQUEST })
        try {
            await axios.delete(`project-legal/${id}`)
                .then(response => {
                    return response.data
                })
                .then(data => {
                    if (data.status === "success") {
                        resolve(data)
                    }else{
                        rejected(data)
                    }
                })
                .catch(error => {
                    resolve(error)
                })
        } catch (err) {
            rejected(err)
            dispatch({
                type: ProjectLegalConstants.FETCH_PROJECT_LEGAL_POST_FAILURE,
                payload: { error: "Terjadi kesalahan mohon ulangi beberapa saat lagi" }
            })
            rejected("Terjadi kesalahan mohon ulangi beberapa saat lagi")
        }
    })
}

export const updateProjectLegal = (id,payload) => async (dispatch) => {
    return new Promise(async (resolve, rejected) => {
        dispatch({ type: ProjectLegalConstants.FETCH_PROJECT_LEGAL_POST_REQUEST })
        try {
            await axios.patch(`project-legal/${id}`,payload)
                .then(response => {
                    return response.data
                })
                .then(data => {
                    console.log('data', data);
                    if (data.status === "success") {
                        resolve(data)
                    }else{
                        rejected(data)
                    }
                })
                .catch(error => {
                    resolve(error)
                })
        } catch (err) {
            rejected(err)
            dispatch({
                type: ProjectLegalConstants.FETCH_PROJECT_LEGAL_POST_FAILURE,
                payload: { error: "Terjadi kesalahan mohon ulangi beberapa saat lagi" }
            })
            rejected("Terjadi kesalahan mohon ulangi beberapa saat lagi")
        }
    })
}

