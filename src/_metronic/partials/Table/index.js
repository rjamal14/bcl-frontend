/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React from 'react';
import { useTable, usePagination } from 'react-table';
import Pagination from "../../layout/components/pagination";

function Table({ setPage, columns, customState, data, cstmHead, currentpage, totalPage, noPaging }) {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    pageOptions,
    state: { pageIndex, pageSize },
  } = useTable(
    {
      columns,
      data,
      customState,
      useControlledState: (state) => {
        return React.useMemo(
          () => ({
            ...state,
            pageIndex: currentpage,
          }),
          [state, currentpage]
        );
      },
      initialState: { pageIndex: currentpage }, // Pass our hoisted table state
      manualPagination: true, // Tell the usePagination
      // hook that we'll handle our own data fetching
      // This means we'll also have to provide our own
      // pageCount.
      pageCount: totalPage,
    },
    usePagination
  );

  return (
    <>
      <div className={`card`}>
        <div className="card-body py-3">
          <div className="table-responsive">
            <table className='table table-row-bordered table-row-gray-100 align-middle gs-0 gy-3'>
              <thead>
                {headerGroups.map((headerGroup) => (
                  <tr {...headerGroup.getHeaderGroupProps()} className='fw-bolder text-muted'>
                    {headerGroup.headers.slice(0, 1).map((column) => (
                      <th>
                        {cstmHead ? cstmHead : column.render('Header')}
                      </th>
                    ))}
                    {headerGroup.headers.slice(1).map((column) => (
                      <th>
                        {column.render('Header')}
                      </th>
                    ))}
                  </tr>
                ))}
              </thead>
              <tbody>
                {(page.length > 0 ?
                  page.map((row, i) => {
                    prepareRow(row);
                    return (
                      <tr {...row.getRowProps()}>
                        {row.cells.map((cell) => {
                          return (
                            <td>
                              {cell.render('Cell')}
                            </td>
                          );
                        })}
                      </tr>
                    );
                  })
                  :
                  <tr>
                    <td colSpan={headerGroups[0].headers.length} className='td-greyout' style={{ textAlign: 'center', padding: 50 }}>tidak ada data yang ditemukan</td>
                  </tr>)
                }
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <br />
      <br />
      <Pagination
        currentPage={currentpage}
        total_page={parseInt(totalPage)}
        onChange={(val) => {
          setPage(val);
        }}
      />
    </>
  );
}

export default Table;
