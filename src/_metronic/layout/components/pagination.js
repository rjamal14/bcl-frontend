const Pagination = ({ total_page, currentPage, onChange }) => {

    return (
        <>
            <div className='row' style={{
                marginRight: -1,
                display: 'flex',
                alignItems: 'center',
            }}>
                <div className='col-md-3'>
                    <text>Showing Page {currentPage} of {total_page} pages</text>
                </div>
                <div className='col-md-9' style={{
                    display: 'flex',
                    flexDirection: 'row-reverse'
                }}>
                    <div className='row'>
                        <div className='col' style={{
                            padding: 0
                        }}>
                            <button
                                onClick={() => {
                                    onChange(currentPage - 1)
                                }}
                                disabled={currentPage === 1}
                                className='btn'
                                style={{
                                    border: 'solid',
                                    borderRadius: 0,
                                    width: 100,
                                    borderWidth: 1,
                                    borderColor: '#D09C0A'
                                }}
                            >
                                <text style={{ color: '#5C5C5C' }}>Previous</text>
                            </button>
                        </div>
                        <div className='col' style={{
                            padding: 0
                        }}>
                            <button
                                className='btn'
                                style={{
                                    border: 'solid',
                                    borderRadius: 0,
                                    borderWidth: 1,
                                    borderColor: '#D09C0A',
                                    backgroundColor: '#D09C0A',

                                }}
                            >
                                <text style={{ color: '#5C5C5C' }}>{currentPage}</text>
                            </button>
                        </div>
                        <div className='col' style={{
                            padding: 0
                        }}>
                            <button
                                onClick={() => {
                                    onChange(currentPage + 1)
                                }}
                                disabled={currentPage === total_page}
                                className='btn'
                                style={{
                                    border: 'solid',
                                    borderRadius: 0,
                                    width: 100,
                                    borderWidth: 1,
                                    borderColor: '#D09C0A'
                                }}
                            >
                                <text style={{ color: '#5C5C5C' }}>Next</text>
                            </button>
                        </div>
                    </div>
                </div>
            </div >
        </>
    )
}
export default Pagination;
