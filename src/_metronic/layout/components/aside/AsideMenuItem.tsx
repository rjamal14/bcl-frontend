/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable react/jsx-no-target-blank */
/* eslint-disable jsx-a11y/anchor-has-content */
import React from 'react'
import clsx from 'clsx'
import { Link } from 'react-router-dom'
import { useLocation } from 'react-router'
import { checkIsActive, KTSVG } from '../../../helpers'
import { useLayout } from '../../core'
import { toAbsoluteUrl } from '../../../helpers/AssetHelpers'

type Props = {
  to: string
  title: string
  icon?: string
  iconPath?: string
  className?: string
  hasBullet?: boolean
  bsTitle?: string
  outside?: boolean
}

const AsideMenuItem: React.FC<Props> = ({
  children,
  to,
  title,
  icon,
  iconPath,
  className,
  bsTitle,
  outside = false,
  hasBullet = false,
}) => {
  const { pathname } = useLocation()
  const isActive = checkIsActive(pathname, to)
  const { config } = useLayout()
  const { aside } = config

  return (
    <div className={clsx('menu-item', isActive && 'here show', className)}>
      {outside ? (
        <a
          href={to}
          target='_blank'
          className={clsx('menu-link menu-center', { active: isActive })}
        >
          {iconPath && aside.menuIcon === 'font' && (
            <div className='d-flex flex-column align-items-center'>
              <span className='menu-icon me-0'>
                <img className='rounded' src={toAbsoluteUrl(iconPath)} width="30" />
              </span>
              <text className={'text-primary'}>{title}</text>
            </div>
          )}
        </a>
      ) : (
        <>
          <Link
            className={clsx('menu-link menu-center', { active: isActive })}
            to={to}
            data-bs-toggle='tooltip'
            data-bs-trigger='hover'
            data-bs-dismiss='click'
            data-bs-placement='right'
            data-bs-original-title={bsTitle}
          >
            {hasBullet && (
              <span className='menu-bullet'>
                <span className='bullet bullet-dot'></span>
              </span>
            )}
            {icon && aside.menuIcon === 'svg' && (
              <span className='menu-icon'>
                <KTSVG path={icon} className='svg-icon-2' />
              </span>
            )}
            {iconPath && aside.menuIcon === 'font' ? (
              <div className={clsx('d-flex flex-column align-items-center menu-list', { active: isActive })}>
                <span className='menu-icon me-0'>
                  <img className='rounded' src={toAbsoluteUrl(iconPath)} width="30" />
                </span>
                <text className={'text-primary'}>{title}</text>
              </div>

            ) : (
              <span className='menu-title'>{title}</span>
            )}
          </Link>
          {children}
        </>
      )}
    </div>
  )
}

export { AsideMenuItem }
