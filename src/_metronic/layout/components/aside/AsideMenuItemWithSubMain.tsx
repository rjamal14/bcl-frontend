/* eslint-disable jsx-a11y/alt-text */
import { FC } from 'react'
import clsx from 'clsx'
import { useLocation } from 'react-router'
import { checkIsActive } from '../../../helpers'
import { useLayout } from '../../core'
import { toAbsoluteUrl } from '../../../helpers/AssetHelpers'

type Props = {
  to: string
  title: string
  icon?: string
  iconPath?: string
  hasBullet?: boolean
  bsTitle?: string
}

const AsideMenuItemWithSubMain: FC<Props> = ({
  children,
  to,
  title,
  icon,
  hasBullet,
  bsTitle,
  iconPath,
}) => {
  const { pathname } = useLocation()
  const isActive = checkIsActive(pathname, to)
  const { config } = useLayout()
  const { aside } = config
  return (
    <div
      className={clsx('menu-item py-3', { 'here show': isActive })}
      data-kt-menu-trigger='click'
      data-kt-menu-placement='right-start'
    >
      <span className={clsx('menu-link menu-center', { active: isActive })}>
        {iconPath && aside.menuIcon === 'font' && (
          <div className={clsx('d-flex flex-column align-items-center menu-list', { active: isActive })}>
            <span className='menu-icon me-0'>
              <img className='rounded' src={toAbsoluteUrl(iconPath)} width="30" />
            </span>
            <text className={'text-primary'}>{title}</text>
          </div>
        )}
      </span>
      <div
        className={clsx('menu-sub menu-sub-dropdown w-225px w-lg-275px px-1 py-4', {
          'menu-active-bg': isActive,
        })}
      >
        {children}
      </div>
    </div>
  )
}

export { AsideMenuItemWithSubMain }
