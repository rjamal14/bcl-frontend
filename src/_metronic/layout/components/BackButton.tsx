import React from 'react'
import { useNavigate } from 'react-router-dom'
import { KTSVG } from '../../helpers'

const BackButton: React.FC = () => {
  const navigate = useNavigate()

  return (
    <button className='btn btn-primary h-100 me-3' onClick={() => navigate(-1)}>
      <KTSVG path='/media/icon/arrow-back.svg' className='svg-icon-3' /><span className='indicator-label'>Back</span>
    </button>)
}

export default BackButton