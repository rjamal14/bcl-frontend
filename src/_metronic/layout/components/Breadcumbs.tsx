import React from 'react'

type Props = {
  data: any[],
  components: any
}

const Breadcumbs: React.FC<Props> = ({ data, components }) => {
  return (
    <div className={`card mb-5`}>
      <div className='card-body py-5'>
        <div className="float-start">
          <h3>{data[data.length - 1].title}</h3>
          {data.map((item: { title: string }, i: number) => {
            const isFirst = i != data.length - 1
            return (item.title + ` ` + (isFirst ? ` / ` : ``))
          })}
        </div>
        <div className="float-end align-middle">
          {components}
        </div>
      </div>
    </div>)
}

export default Breadcumbs