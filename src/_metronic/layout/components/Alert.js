import { Alert } from "react-bootstrap";

const AlertCustom = ({ show, setShow }) => {

    if(show){
        return (
            <>
                <Alert variant={show === 'error' ? 'danger' : 'primary' } onClose={() => setShow(null)} dismissible>
                    <p></p>
                    <Alert.Heading>{show !== 'error' ? 'Berhasil' : 'Gagal'} !</Alert.Heading>
                    <p>Proyek {show !== 'error' ? 'berhasil' : 'gagal'} {show === 'success' ? 'disimpan' : 'dihapus'} !</p>
                </Alert>
            </>
        )
    }else{
        return null
    }

}
export default AlertCustom;
