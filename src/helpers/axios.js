import axios from 'axios';
const API_URL = process.env.REACT_APP_BASE_URL

const axiosIntance = axios.create({
    baseURL: API_URL,
});

axiosIntance.interceptors.request.use((req) => {
    var token = localStorage.getItem("token");
    if(token){
        req.headers.Authorization = `Bearer ${token}`;
    }
    return req;
})

axiosIntance.interceptors.response.use((res) => {
    return res;
}, (error) => {
    const status = error.response ? error.response.status : 500;
    if(status && status === 500){
        localStorage.clear();
    }
    return Promise.reject(error);
})

export default axiosIntance;